# [Jquery.heightFromBG](https://bitbucket.org/tomeralmog/jquery.jquery.heightfrombg/)

Set element height from CSS background image dimensions.

##Requires

* JQuery


##Installation
Include script after the jQuery library (unless you are packaging scripts somehow else):

	
    <script src="/path/to/jquery.jquery.heightFromBG.min.js"></script>


##Usage
HTML MARKDOWN:

    <div class="show-bg"></div>
JQUERY

    $(document).heightFromBG(options);


Options:

- selector:       
	- default:'.show-bg'    
	- description: the class selector





## Author


[Tomer Almog](https://bitbucket.org/tomeralmog)

## License
[MIT](http://en.wikipedia.org/wiki/MIT_License)