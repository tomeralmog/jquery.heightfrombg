/*
jQuery heightFromBG 1.0.0
Copyright (c) 2014 Tomer Almog
License: MIT
*/


(function ( $ ) {
	$.fn.heightFromBG = function(options) {
		
		 var settings = $.extend({
		// These are the defaults.
			selector: '.show-bg',					
		
		}, options );
		
		
		$(window).resize(function(){
			setBGSize();
		})
		
		
		 return this.each(function() {
			
									
			var bgImages = new Array();
			function getBGSize(){
				var def = $.Deferred();
				var totalBG = $('.show-bg').length;
				console.log(totalBG);
				$(settings.selector).each(function(j,item){
					
					var w = $(this).width();
					var $that = $(this);
					var url = $(this).css('background-image').replace('url(', '').replace(')', '').replace("'", '').replace('"', '').replace('"', '');			
					var bgImg = $('<img />');
					bgImg.hide();
					bgImg.bind('load', function()
					{
						console.log(bgImages);
						bgImages.push({
							elem:$that,
							height:	$(this).height(),
							width:$(this).width()
						});//$(this).height(), $(this).width());
						console.log(bgImages);
						if(j == totalBG-1){
							def.resolve();
						}
					});
					$(this).append(bgImg);
					bgImg.attr('src', url);
					//$(this).remove(bgImg);
					
				});
				return def;
				
			}
			
			function setBGSize(){
	
				for(var i=0;i<bgImages.length; i++){
					bgImages[i].elem.css('height', (bgImages[i].height / bgImages[i].width ) * bgImages[i].elem.width());
					
				}	
			}
		
		
						
		
		getBGSize().done(setBGSize);
			
		
		});
	};
}( jQuery ));

